Como Usar

1 - Instalar / Configurar o Maven :
	
	https://maven.apache.org/users/index.html
	
	https://maven.apache.org/download.cgi

2 - Clonar o Projeto

3 - Executar o empacotamento

	- mvn clean install
	
4 - Executar o Jar

	- java -jar target/BalancedBracketsTest.jar {PARAMETRO}
	
	Exemplo:
	
	java -jar target/BalancedBracketsTest.jar '((([]{])))[]{}'
	
	java -jar target/BalancedBracketsTest.jar '()[]{}'