package br.com.rodrigo;

import java.util.Stack;

public class App {

	public static void main(String[] args) {
		if (args != null && args.length > 0 && args[0].length() > 0) {
			Boolean ret = isBalanced(args[0].toString());
			System.out.println( args[0] + (ret ? " is valid" : " is not valid"));
		} else {
			System.out.println("Informar um valor");
		}
		
	}
	
	private static Boolean isBalanced(String input) {

	    Stack<Character> stack = new Stack<Character>();
	    for (int i = 0; i < input.length(); i++) {
	        Character ch = input.charAt(i);
	        if (input.charAt(i) == '{' || input.charAt(i) == '[' || input.charAt(i) == '(') {
	            stack.push(input.charAt(i));
	        } else {
	            if (stack.isEmpty() 
	             || (stack.peek() == '[' && ch != ']')
	             || (stack.peek() == '{' && ch != '}')
	             || (stack.peek() == '(' && ch != ')')) {
	                return false;
	            } else {
	                stack.pop();
	            }
	        }
	    }
	    
	    if (stack.empty()) {
	        return true;
	    }
	    
	    return false;

	}

}
